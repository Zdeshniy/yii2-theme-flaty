Yii2  Flaty Theme 
======================
Theme for Yii2 Web Applicaiton

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kongoon/yii2-theme-flaty "*"
```

or add

```
"kongoon/yii2-theme-flaty": "*"
```

to the require section of your `composer.json` file.


Usage
-----
```
use kongoon\theme\flaty;

flaty\FlatyAsset::register($this);
```